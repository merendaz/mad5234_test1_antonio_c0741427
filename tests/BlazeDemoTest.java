import static org.junit.Assert.*;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BlazeDemoTest {
	
	/*
    * Test website: http://blazedemo.com
    * Ids:
    *  - Modal: a.exit
    *  - Name of section: .click-before-outline
    *  - First name: #firstname2
    *  - E-mail address: #email2
    *  - Postal Code: #postalcode2
    *  - Subscribe btn: #g-recaptcha-btn-2
    */

	// -----------------------------------
	// Configuration variables
	// -----------------------------------
	// Location of chromedriver file
	final String CHROMEDRIVER_LOCATION = "/Users/merendaz/Downloads/chromedriver";
	// Website we want to test
	final String URL_TO_TEST = "http://blazedemo.com";
			
	// -----------------------------------
	// Global driver variables
	// -----------------------------------
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		// -------------------------------------------------
    	//Here goes the code to be executed before each Test
    	// -------------------------------------------------
		// 1. Selenium setup
		System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_LOCATION);
		driver = new ChromeDriver();
		// 2. go to website
		driver.get(URL_TO_TEST);
	}

	@After
	public void tearDown() throws Exception {
	// ------------------------------------------------
   	//Here goes the code to be executed after each Test
   	// ------------------------------------------------
   	//1. Pause for 5 seconds before closing the browser
       Thread.sleep(5000);
       driver.close();
	}

	@Test
	public void testTC1() {
		// --------------------------------------------
    	// TC1:  There are 7 departure cities
    	// --------------------------------------------
		// 1. Selector:
		// "select[name='fromPort'] > option"
		List<WebElement> departCities = driver.findElements(
				By.cssSelector("select[name='fromPort'] > option"));
        assertEquals(7, departCities.size());
	}
	
	@Test
	public void testTC2() {
		// --------------------------------------------
    	// TC2:  The Details of Flight #12 of Virgin 
		// America, From Paris to Buenos Aires are:
		// Departs: 11:23 AM
		// Arrives:	1:45 PM
		// Price: $765.32
    	// --------------------------------------------
		
		//Selectors:
		// "input.btn.btn-primary"
		// "tbody > tr"
		// "td"
		
		// 1.Find the button Find flights to click and go to the next page
		WebElement findFlightButton = driver.findElement(By.cssSelector("input.btn.btn-primary"));
		findFlightButton.click();
		
		// 2. Fetching the lines of the table
		List<WebElement> findFlights = driver.findElements(By.cssSelector("tbody > tr"));
		
		// 3. Fetching the elements of the line of interest (4th line):
		List<WebElement> flight12Details = findFlights.get(3).findElements(By.cssSelector("td"));
		
		// 4. Fetching the elements and checking
		// 4a. Depart time:
		String departs = flight12Details.get(3).getText();
		assertEquals("11:23 AM", departs);
		
		// 4b. Arrive time:
		String arrives = flight12Details.get(4).getText();
		assertEquals("1:45 PM", arrives);
		
		// 4c. Price:
		String price = flight12Details.get(5).getText();
		assertEquals("$765.32", price);
 	}
	
	@Test
	public void testTC3() throws InterruptedException, ParseException{
	
		// --------------------------------------------
		// TC3:  After purchasing a flight, the next 
		// page will show an id code with 13 digits
		// and the date shows today
		// --------------------------------------------
	
		//Selectors:
		// "input.btn.btn-primary"
		// ".btn.btn-small"
		// "td"
		
		// 1. Find the button Find flights to click and go to the next page
		WebElement findFlightButton = driver.findElement(By.cssSelector("input.btn.btn-primary"));
		findFlightButton.click();
		
		// 2. Find the button Choose flight to click and go to the next page
		WebElement chooseFlightButton = driver.findElement(By.cssSelector(".btn.btn-small"));
		chooseFlightButton.click();
		
		// 3. Find the button purchse flight to click and go to the next page
		WebElement purchaseFlightButton = driver.findElement(By.cssSelector("input.btn.btn-primary"));
		purchaseFlightButton.click();
		
		// 4. Fetching the lines of the table
		List<WebElement> flightPurchased = driver.findElements(By.cssSelector("tbody > tr"));
		
		// 5. Fetching the elements and checking:
		// 5a. First line => Id:
		List<WebElement> flightId = flightPurchased.get(0).findElements(By.cssSelector("td"));
		char[] flightIdArray = (flightId.get(1).getText()).toCharArray();
		int idActual = flightIdArray.length;
		assertEquals(13,idActual);
		
		// 5b. Seventh line => flightDate:
		List<WebElement> flightDate = flightPurchased.get(6).findElements(By.cssSelector("td"));
		String dateActual = (flightDate.get(1).getText()).substring(0, 16);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E, d MMM yyyy");  
		LocalDateTime now = LocalDateTime.now();
		assertEquals(dtf.format(now), dateActual);
	}

}
