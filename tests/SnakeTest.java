import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class SnakeTest {
	
	// -----------------------------------
	// Global variables
	// -----------------------------------
		Snake snake1;
		Snake snake2;

	@Before
	public void setUp() throws Exception {
		
	// -------------------------------------------------
   	//Here goes the code to be executed before each Test
   	// -------------------------------------------------	
		/**
		 * Creates a new Snake object.
		 * @param name			the name of the snake
		 * @param length		length of the snake
		 * @param favoriteFood	what the snake likes to eat
		 */
		
		// 1. Create 2 new snake objects
		// Snake(String name, int length, String favoriteFood)
		snake1 = new Snake("Peter", 10, "coffee");
		snake2 = new Snake("Takis", 80, "vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testTC1() {
	/**
	 * Snakes are healthy if they love vegetables
	 * @return true if snake is healthy 
	 * assertTrue(a)
	 */
		
	//1. Get the actual output of isHealthy for the two snakes
	boolean a1 = snake1.isHealthy();
	
	boolean a2 = snake2.isHealthy();
		
	//2. Do the assertTrue
	assertTrue(!a1&&a2);
	}
	
	@Test
	public void testTC2() {
	/**
	 * Checks if snake will fit in a cage 
	 * @return true if the length of the snake is less than the cage length
	 */
	// 1. Geting  the actual output for each Scenario
	
	// 1a. Cage Length < Snake Length  (Expected to be false)
	boolean cl1 = snake1.fitsInCage(5);
	
	// 1b. Cage Length == Snake Length (Expected to be false)
	boolean cl2 = snake1.fitsInCage(10);
	
	// 1c. Cage Length > Snake Length  (Expected to be true)
	boolean cl3 = snake1.fitsInCage(15);
	
	//2. Do the assertTrue
	assertTrue(!cl1&&!cl2&&cl3);
	}
}
